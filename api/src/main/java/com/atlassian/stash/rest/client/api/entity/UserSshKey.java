package com.atlassian.stash.rest.client.api.entity;

/**
 * SSH public key for user
 */
public class UserSshKey extends SshKey {

    public UserSshKey(final long id, final String text, final String label) {
        super(text, label, id);
    }

}
