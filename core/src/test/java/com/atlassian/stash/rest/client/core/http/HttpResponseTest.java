package com.atlassian.stash.rest.client.core.http;

import com.google.common.collect.ImmutableMap;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.InputStream;

import static org.hamcrest.CoreMatchers.is;

public class HttpResponseTest {

    @Test
    public void testGetContentEncodingHeaderValue() throws Exception {
        // given
        HttpResponse response = new HttpResponse(0, "", ImmutableMap.of("CoNtEnT-TyPe", "application/json ; charset=UTF-8"), Mockito.mock(InputStream.class));
        
        // when
        String encoding = response.getContentEncoding(null);
        
        // then
        Assert.assertThat(encoding, is("UTF-8"));
    }

    @Test
    public void testGetContentEncodingDefaultValue() throws Exception {
        // given
        HttpResponse response = new HttpResponse(0, "", ImmutableMap.of("content-type", "application/json"), Mockito.mock(InputStream.class));

        // when
        String encoding = response.getContentEncoding("UTF-8");

        // then
        Assert.assertThat(encoding, is("UTF-8"));
    }

    @Test
    public void testGetContentTypeWithEncoding() throws Exception {
        // given
        HttpResponse response = new HttpResponse(0, "", ImmutableMap.of("content-type", "application/json ; charset=UTF-8"), Mockito.mock(InputStream.class));

        // when
        String encoding = response.getContentType();

        // then
        Assert.assertThat(encoding, is("application/json"));
    }

    @Test
    public void testGetContentTypeWithoutEncoding() throws Exception {
        // given
        HttpResponse response = new HttpResponse(0, "", ImmutableMap.of("content-type", "application/json"), Mockito.mock(InputStream.class));

        // when
        String encoding = response.getContentType();

        // then
        Assert.assertThat(encoding, is("application/json"));
    }
}
